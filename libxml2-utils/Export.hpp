// file      : libxml2-utils/Export.hpp
// copyright : Copyright (c) 2019 Alexander Gnatyuk
// license   : MIT; see accompanying LICENSE file

#pragma once

#ifdef LIBXML2_UTILS_USE_MODULES

#define LIBXML2_UTILS_EXPORT_START export {
#define LIBXML2_UTILS_EXPORT_END }

#else

#define LIBXML2_UTILS_EXPORT_START
#define LIBXML2_UTILS_EXPORT_END

#endif

